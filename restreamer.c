#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
  if (argc < 4 || argc % 2 != 0) {
    exit(EXIT_FAILURE);
  }

  const int fd = socket(AF_INET, SOCK_DGRAM, 0);

  if (fd < 0) {
    perror("socket(2)");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_in sin = {.sin_family = AF_INET,
                            .sin_addr = {.s_addr = INADDR_ANY},
                            .sin_port = htons(strtol(argv[1], NULL, 10))};

  if (bind(fd, (const struct sockaddr *)&sin, sizeof(sin)) < 0) {
    perror("bind(2)");
    exit(EXIT_FAILURE);
  }

  const int no_clients = (argc - 2) / 2;
  struct sockaddr_in client_addrs[no_clients];

  for (int i = 2, c = 0; i < argc; i += 2, ++c) {
    client_addrs[c].sin_family = AF_INET;
    client_addrs[c].sin_port = htons(strtol(argv[i + 1], NULL, 10));

    if (inet_pton(AF_INET, argv[i], &(client_addrs[c].sin_addr)) != 1) {
      perror("inet_pton(3)");
      exit(EXIT_FAILURE);
    }
  }

  char buf[2048];

  while (true) {
    const ssize_t len = recv(fd, buf, sizeof(buf), 0);

    if (len <= 0) {
      perror("recv(2)");
      exit(EXIT_FAILURE);
    }

    for (int i = 0; i < no_clients; ++i) {
      ssize_t sent =
          sendto(fd, buf, len, 0, (const struct sockaddr *)(&client_addrs[i]),
                 sizeof(sin));

      if (sent != len && errno != ECONNREFUSED) {
        perror("sendto(2)");
        exit(EXIT_FAILURE);
      }
    }
  }
}